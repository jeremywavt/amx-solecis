PROGRAM_NAME='Solecis'
(***********************************************************)
(***********************************************************)
(*  FILE_LAST_MODIFIED_ON: 04/05/2006  AT: 09:00:25        *)
(***********************************************************)

DEFINE_DEVICE

dvMaster	=	0:0:0


dvSolecis	=	5004:1:0
dvDXRX		=	7004:1:0
dvTP			=	10002:1:0

DEFINE_CONSTANT

//Touchpanel Buttons
bHDMI1	=	1
bHDMI2	=	2
bHDMI4	=	3
bVGA3	=	4
bVGA5	=	5
bVideoMute=	6
cTextOutput	=	40

//Solecis Inputs
SolecisHDMI1		=	1
SolecisHDMI2		=	2
SolecisVGA3			=	3
SolecisHDMI4		=	4
SolecisVGA5			=	5

//solecis Outputs
SolecisOUT 			=	1
SolecisVidMute	=	0

DEFINE_VARIABLE

volatile integer nInputSource

DEFINE_START

//Function to switch Solecis (LAZY)
define_function fnDoSwitch (integer INPUT, integer OUTPUT)
{
	send_command dvSolecis, "'VI',itoa(INPUT),'O',itoa(OUTPUT)"
}


DEFINE_EVENT

//HDMI Input Button Events

//HDMI Input 1
button_event [dvTP, bHDMI1]
{
	push:
	{
		fnDoSwitch (SolecisHDMI1,SolecisOUT)
		nInputSource = 1
	}
}

//HDMI Input 2
button_event [dvTP, bHDMI2]
{
	push:
	{
		fnDoSwitch (SolecisHDMI2,SolecisOUT)
		nInputSource = 2
	}
}

//HDMI Input 4
button_event [dvTP, bHDMI4]
{
	push:
	{
		fnDoSwitch (SolecisHDMI4,SolecisOUT)
		nInputSource = 4
	}
}

//VGA Input 3
button_event [dvTP, bVGA5]
{
	push:
	{
		fnDoSwitch (SolecisVGA5,SolecisOUT)
		nInputSource = 5
	}
}

//VGA Input 5
button_event [dvTP, bVGA3]
{
	push:
	{
		fnDoSwitch (SolecisVGA3,SolecisOUT)
		nInputSource = 3
	}
}

//Video Mute
button_event [dvTP, bVideoMute]
{
	push:
	{
		fnDoSwitch (SolecisHDMI4,SolecisVidMute)
		nInputSource = 6
	}
}

(***********************************************************)
(*            THE ACTUAL PROGRAM GOES BELOW                *)
(***********************************************************)
DEFINE_PROGRAM

//Button Feedback
[dvTP, bHDMI1] = (nInputSource == 1)
[dvTP, bHDMI2]	=	(nInputSource == 2)
[dvTp, bHDMI4] = (nInputSource == 4)

[dvTP, bVGA3] = (nInputSource == 3)
[dvTP, bVGA5]	=	(nInputSource == 5)


[dvTP, bVideoMute] = (nInputSource == 6)


(***********************************************************)
(*                     END OF PROGRAM                      *)
(*        DO NOT PUT ANY CODE BELOW THIS COMMENT           *)
(***********************************************************)

